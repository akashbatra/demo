**************************
*Demo spring boot project*
**************************

Project Description: 
This project demonstrates the simple CRUD operations for a sample entity(Product).

Requirements to setup project:

1) JAVA - Spring Boot Framework.
2) MySQL
3) GIT
4) Eclipse
5) Maven


API Description:
Base Url - http://localhost:8080

1) Save Product Details: 
End point - /product/save 
Method Type - POST
Input body - 
{	"productName":<product name>,
	"productDescription": <product description>
}

Output -

{
    "status": "<Success/Failure>",
    "responseMessage": "<Success/Failure Message>"
}

2) Update Product: 
End point - /product/update 
Method Type - POST
Input body - 
{	"productName":<product name>,
	"productDescription": <product description>
}

Output -

{
    "status": "<Success/Failure>",
    "responseMessage": "<Success/Failure Message>"
}

3) Get Product Details: 
End point - /product/getProductDetails?productName=<product name>
 
Method Type - GET
Output -

{
     "status": "<Success/Failure>",
    "productName": <product name>,
    "productDescription": <product description>,
    "entryDate": <entry date>
}

4) Delete Product Details: 
End point - /product/delete?productName=<product name>
Method Type - DELETE
Output -

{
    "status": "<Success/Failure>",
    "responseMessage": "<Success/Failure Message>"
}


