package com.evon.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.evon.demo.bean.ProductBean;
import com.evon.demo.common.Constants;
import com.evon.demo.dto.ProductDetailsResponseDto;
import com.evon.demo.dto.ResponseDto;
import com.evon.demo.service.IProductDetailsService;

@RestController
@RequestMapping(value = "/product")
public class ProductController { 
	 
	 @Autowired
	 private IProductDetailsService userDetailsService;
	 
	 @PostMapping(value = "/save",headers = "Accept=application/json")
	 public ResponseEntity<ResponseDto> save(@RequestBody ProductBean product){	
		 ResponseDto responseDto = userDetailsService.saveProduct(product);
		 HttpStatus status = HttpStatus.BAD_REQUEST;
		 if(responseDto.getStatus().equalsIgnoreCase(Constants.SUCCESS_STATUS)){
			 status= HttpStatus.OK;
		 };
		 return new ResponseEntity<ResponseDto>(responseDto,status);				 
	 }
	 
	 @GetMapping(value = "/getProductDetails",headers = "Accept=application/json")
	 public ResponseEntity<ProductDetailsResponseDto> getProductDetails(@RequestParam String productName){		 		
		 ProductDetailsResponseDto responseDto = userDetailsService.getProductDetails(productName);
		 HttpStatus status = HttpStatus.BAD_REQUEST;
		 if(responseDto.getStatus().equalsIgnoreCase(Constants.SUCCESS_STATUS)){
			 status= HttpStatus.OK;
		 };
		 return new ResponseEntity<ProductDetailsResponseDto>(responseDto,status);		 	 	 
	 }	
	 
	 @PostMapping(value = "/update", headers = "Accept=application/json")
	 public ResponseEntity<ResponseDto> update(@RequestBody ProductBean product){		 
		 ResponseDto responseDto = userDetailsService.updateProduct(product);
		 HttpStatus status = HttpStatus.BAD_REQUEST;
		 if(responseDto.getStatus().equalsIgnoreCase(Constants.SUCCESS_STATUS)){
			 status= HttpStatus.OK;
		 };
		 return new ResponseEntity<ResponseDto>(responseDto,status);		 	 
	 }
	 
	 @DeleteMapping(value = "/delete",headers = "Accept=application/json")
	 public ResponseEntity<ResponseDto> delete(@RequestParam String productName){		 		
		 ResponseDto responseDto = userDetailsService.deleteProduct(productName);
		 HttpStatus status = HttpStatus.BAD_REQUEST;
		 if(responseDto.getStatus().equalsIgnoreCase(Constants.SUCCESS_STATUS)){
			 status= HttpStatus.OK;
		 };
		 return new ResponseEntity<ResponseDto>(responseDto,status);			 	 
	 }		 
}
