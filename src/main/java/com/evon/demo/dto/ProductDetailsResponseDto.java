package com.evon.demo.dto;

public class ProductDetailsResponseDto extends ResponseDto{
	
	private String productName;
	private String productDescription;
	private String entryDate;
	
	public ProductDetailsResponseDto(){}
	
	public ProductDetailsResponseDto(String productName,
			String productDescription, String entryDate) {
		super();
		this.productName = productName;
		this.productDescription = productDescription;
		this.entryDate = entryDate;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

}
