package com.evon.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.evon.demo.entity.Product;

public interface ProductDetailsRepository extends CrudRepository<Product, Long> {
	
	public Product findByProductName(String productName);
	

}
