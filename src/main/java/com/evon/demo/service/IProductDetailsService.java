package com.evon.demo.service;

import com.evon.demo.bean.ProductBean;
import com.evon.demo.dto.ProductDetailsResponseDto;
import com.evon.demo.dto.ResponseDto;

public interface IProductDetailsService {

	public ResponseDto saveProduct(ProductBean productDetails);
	public ResponseDto updateProduct(ProductBean productDetails);
	public ProductDetailsResponseDto getProductDetails(String productName);
	public ResponseDto deleteProduct(String productName);
	

}
