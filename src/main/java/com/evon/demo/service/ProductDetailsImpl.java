package com.evon.demo.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.evon.demo.bean.ProductBean;
import com.evon.demo.common.Constants;
import com.evon.demo.dto.ProductDetailsResponseDto;
import com.evon.demo.dto.ResponseDto;
import com.evon.demo.entity.Product;
import com.evon.demo.repository.ProductDetailsRepository;

@Service
public class ProductDetailsImpl implements IProductDetailsService {
	private static final Logger LOG = LoggerFactory.getLogger(ProductDetailsImpl.class);
	
	@Autowired
	private ProductDetailsRepository productDetailsRepo;
	
	/**
	 * This service method is used to save product details in Database.
	 */
	@Override
	@Transactional
	public ResponseDto saveProduct(ProductBean productDetails) {
		ResponseDto response = new ResponseDto();		
		try {
			Product product = productDetailsRepo.findByProductName(productDetails.getProductName());
			
			if(product != null){
				response.setStatus(Constants.FAILURE_STATUS);
				response.setResponseMessage("Duplicate product Name not allowed.");
							
			}else{
				product = new Product(productDetails.getProductName(), productDetails.getProductDescription(), new Date());		
				productDetailsRepo.save(product);
				response.setStatus(Constants.SUCCESS_STATUS);
				response.setResponseMessage("Product saved successfully.");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			response.setStatus(Constants.FAILURE_STATUS);
			response.setResponseMessage(e.getMessage());
			
		}
		return response;
				
	}
	
	/**
	 * This service method is used to fetch product details in Database.
	 */
	@Override
	public ProductDetailsResponseDto getProductDetails(String productName) {
		ProductDetailsResponseDto productDetailsResponse = new ProductDetailsResponseDto();
		Product product = productDetailsRepo.findByProductName(productName);
		if(product == null){
			productDetailsResponse.setStatus(Constants.FAILURE_STATUS);
			productDetailsResponse.setResponseMessage("Please enter a valid product name.");
		}else{
			productDetailsResponse = new ProductDetailsResponseDto(product.getProductName(), product.getProductDescription(), product.getDateCreated().toString());
			productDetailsResponse.setStatus(Constants.SUCCESS_STATUS);		
		}		
		return productDetailsResponse;			
	}
	
	/**
	 * This service method is used to update product details in Database.
	 */
	@Override
	@Transactional
	public ResponseDto updateProduct(ProductBean productDetails) {		
		ResponseDto response = new ResponseDto();	
		try {
			Product product = productDetailsRepo.findByProductName(productDetails.getProductName());
			if(product == null){
				response.setStatus(Constants.FAILURE_STATUS);
				response.setResponseMessage("Please enter a valid product name.");
			}else{
				productDetailsRepo.save(product);
				response.setStatus(Constants.SUCCESS_STATUS);
				response.setResponseMessage("Product description updated successfully.");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			response.setStatus(Constants.FAILURE_STATUS);
			response.setResponseMessage(e.getMessage());
		}
		return response;				
	}
	
	/**
	 * This service method is used to delete product from Database.
	 */
	@Override
	@Transactional
	public ResponseDto deleteProduct(String productName) {
		ResponseDto response = new ResponseDto();	
		try {
			Product product = productDetailsRepo.findByProductName(productName);
			if(product == null){
				response.setStatus(Constants.FAILURE_STATUS);
				response.setResponseMessage("Please enter a valid product name.");
			}else{
				productDetailsRepo.delete(product);
				response.setStatus(Constants.SUCCESS_STATUS);
				response.setResponseMessage("Product deleted successfully.");
			}
		} catch (Exception e) {
			LOG.error(e.getMessage());
			response.setStatus(Constants.FAILURE_STATUS);
			response.setResponseMessage(e.getMessage());
		}
		return response;		
	}
}
